from django.urls import path, include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('confirm', views.confirm, name='confirm'),
    path('cases', views.cases, name='cases'),
    path('about-us', views.aboutus, name='about-us'),
    path('donations', views.donate_list, name='donasi'),
    path('contact-us', views.contactus, name='contact-us'),
]