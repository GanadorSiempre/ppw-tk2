from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver

from . import views
from .views import home, confirm, cases, aboutus, contactus, donate_list
from donation.models import Donate

'''
@tag('functional')
class FunctifonalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
'''

class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.

    def test_root_about_us_exists(self):
        response = Client().get('/about-us')
        self.assertEqual(response.status_code, 200)   

    def test_root_cases_exists(self):
        response = Client().get('/cases')
        self.assertEqual(response.status_code, 200)
    
    def test_root_contactus_exists(self):
        response = Client().get('/contact-us')
        self.assertEqual(response.status_code, 200)
    
    def test_root_confirm_exists(self):
        response = Client().get('/confirm')
        self.assertEqual(response.status_code, 200)
    
    def test_template_home(self):
         response = Client().get('/')
         self.assertTemplateUsed(response, 'home.html')

    def test_template_aboutus(self):
         response = Client().get('/about-us/')
         self.assertTemplateUsed(response, 'aboutus.html')

    def test_template_cases(self):
         response = Client().get('/cases/')
         self.assertTemplateUsed(response, 'cases.html')

    def test_template_confirm(self):
         response = Client().get('/confirm', follow=True)
         self.assertTemplateUsed(response, 'confirm.html')

    def test_template_contactus(self):
         response = Client().get('/contact-us', follow=True)
         self.assertTemplateUsed(response, 'contactus.html')

    def test_template_donasi(self):
         response = Client().get('/donations/')
         self.assertTemplateUsed(response, 'donasi.html')

    def test_func_home(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_func_aboutus(self):
        found = resolve('/about-us')
        self.assertEqual(found.func, aboutus)
    
    def test_func_cases(self):
        found = resolve('/cases')
        self.assertEqual(found.func, cases)
    
    def test_func_confirm(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirm)

    def test_func_contactus(self):
        found = resolve('/contact-us')
        self.assertEqual(found.func, contactus)
    
    def test_func_donasi(self):
        found = resolve('/donations')
        self.assertEqual(found.func, donate_list)

