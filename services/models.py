from django.db import models

# Create your models here.
class Service(models.Model):
    name = models.CharField(max_length=160)
    address = models.CharField(max_length=200)
    phone = models.IntegerField(max_length=15)
    detail = models.CharField(max_length=500, default='')