from django import forms
from django.forms import ModelForm,  Textarea, TextInput

class serviceforms(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class' : 'form-control'
    }
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Nama', max_length=160, required=True)
    address = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label = 'Alamat', max_length=200, required=True)
    phone = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label = 'Telepon',max_length=15, required=True)
    detail = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label = 'Detail', max_length=500, required=True)
