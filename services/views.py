from django.shortcuts import render, redirect
from django.http import HttpResponse

from services.models import Service
from services.forms import serviceforms

# Create your views here.
def home(request):
    form_service = serviceforms()
    data = Service.objects.all()
    response = {'form_service' : form_service, 'data' : data}
    return render(request, 'services.html', response)

def post_service(request):
    if request.method == 'POST':
        form = serviceforms(request.POST or None)
        response_data = {}
        if form.is_valid():
            response_data['name'] = request.POST['name']
            response_data['address'] = request.POST['address']
            response_data['phone'] = request.POST['phone']
            response_data['detail'] = request.POST['detail']

            data_service = Service(name=response_data['name'],
                                   address=response_data['address'],
                                   phone=response_data['phone'],
                                   detail=response_data['detail'],
                                   )
            data_service.save()
            return redirect('main:confirm')