from django import forms

class donateform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class' : 'form-control'
    }
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Nama ', max_length=50, required=True)
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label = 'Email ', max_length=30, required=True)
    nominal = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label = 'Nominal ',max_length=30, required=True)