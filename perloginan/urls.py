from django.urls import path
from perloginan.views import logIn, signUp, logOut


app_name = 'perloginan'

urlpatterns = [
    path('login/', logIn, name='login'),
    path('signup/', signUp, name='signup'),
    path('logout/', logOut, name='logout'),
]
