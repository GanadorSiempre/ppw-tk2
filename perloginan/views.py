from django.shortcuts import render, redirect
from perloginan.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


# Create your views here.
def logIn(request):
    if request.method == "POST":
        form = LoginForm(data = request.POST)

        if form.is_valid():
            usernameInput = request.POST["username"]
            passwordInput = request.POST["password"]

            print(usernameInput, passwordInput)
            user = authenticate(request, username = usernameInput, password = passwordInput)

            if user is not None:
                login(request, user)
                return redirect('login')

        else:
            messages.error(request, 'Invalid entry')

    else:
        form = LoginForm()

    context = {
        "form" : form,
    }

    return render(request, "login.html", context)

def signUp(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("login")

        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')

    else:
        form = SignUpForm()

    context = {
        'form' : form
    }

    return render(request, "signup.html", context)

def logOut(request):
    logout(request)
    return redirect("login")

