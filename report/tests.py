from django.test import TestCase, Client
from django.urls import resolve, reverse

from . import views
from report.models import Report
from report.views import home, post_report
from report.forms import reportform
# Create your tests here.


class ReportTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get(reverse('report:home'))
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get(reverse('report:home'))
        self.assertTemplateUsed(response, 'report.html')

    def test_func_report(self):
        found = resolve('/report/post')
        self.assertEqual(found.func, post_report)

    def test_func_home(self):
        found = resolve('/report/')
        self.assertEqual(found.func, home)

    def test_model_report(self):
        Report.objects.create(name='waka', address='rumah waka',phone='0812',case='positif', detail='di perumahannya')
        report = Report.objects.all().count()
        self.assertEqual(report, 1)

    def test_form_report(self):
        Client().post('/report/post',data={'name':'bambang', 'address':'rumah bambang','phone':'0812','case':'positif','detail':'di kelurahan citayem'})
        report = Report.objects.filter(name='bambang', address='rumah bambang',phone='0812',case='positif', detail='di kelurahan citayem').count()
        self.assertEqual(report, 1)

    def test_form_invalid(self):
        form = reportform(data={'name':'', 'address':'','case':'','phone':'','detail':''})
        self.assertFalse(form.is_valid())
        response = Client().get(reverse('report:home'))
        self.assertTemplateUsed(response, 'report.html')
        