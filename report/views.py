from django.shortcuts import render, redirect
from django.http import HttpResponse

from report.models import Report
from report.forms import reportform

# Create your views here.
def home(request):
    form_report = reportform()
    data = Report.objects.all()
    response = {'form_report' : form_report, 'data' : data}
    return render(request, 'report.html', response)


def post_report(request):
    if request.method == 'POST':
        form = reportform(request.POST or None)
        response_data = {}
        if form.is_valid():
            response_data['name'] = request.POST['name']
            response_data['address'] = request.POST['address']
            response_data['case'] = request.POST['case']
            response_data['phone'] = request.POST['phone']
            response_data['detail'] = request.POST['detail']
            data_report = Report(name=response_data['name'],
                                   address=response_data['address'],
                                   phone=response_data['phone'],
                                   detail=response_data['detail'],
                                   case=response_data['case'],
                                   )
            data_report.save()
            return redirect('main:confirm')