from django.test import TestCase, Client
from django.urls import resolve, reverse

from . import views
from feedback.models import Feedback
from feedback.views import home, post_feedback
from feedback.forms import feedbackform
# Create your tests here.


class FeedbackTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get(reverse('feedback:home'))
        self.assertEqual(response.status_code, 200)

    # def test_root_url_status_200_post(self):
    #     response = Client().get(reverse('feedback:post'))
    #     self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get(reverse('feedback:home'))
        self.assertTemplateUsed(response, 'feedback.html')

    def test_func_feedback(self):
        found = resolve('/feedback/post')
        self.assertEqual(found.func, post_feedback)

    def test_func_home(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, home)

    def test_model_feedback(self):
        Feedback.objects.create(name="waka", email="wakawaka@email.com", message="halo")
        feedback = Feedback.objects.all().count()
        self.assertEqual(feedback, 1)

    def test_form_feedback(self):
        Client().post('/feedback/post',data={'name':'bambang', 'email':'bambang@mail.com','message':'bagus'})
        feedback = Feedback.objects.filter(name='bambang', email='bambang@mail.com',message='bagus').count()
        self.assertEqual(feedback, 1)

    def test_form_invalid(self):
        form = feedbackform(data={'name':'', 'email':'','message':''})
        self.assertFalse(form.is_valid())
        response = Client().get(reverse('feedback:home'))
        self.assertTemplateUsed(response, 'feedback.html')
        