from django.shortcuts import render, redirect
from django.http import HttpResponse

from feedback.models import Feedback
from feedback.forms import feedbackform

# Create your views here.
def home(request):
    form_feedback = feedbackform()
    data = Feedback.objects.all()
    response = {'form_feedback' : form_feedback, 'data' : data}
    return render(request, 'feedback.html', response)



def post_feedback(request):
    if request.method == 'POST':
        form = feedbackform(request.POST or None)
        response_data = {}
        if form.is_valid():
            response_data['name'] = request.POST['name']
            response_data['email'] = request.POST['email']
            response_data['message'] = request.POST['message']

            data_feedback = Feedback(name=response_data['name'],
                                   email=response_data['email'],
                                   message=response_data['message'],
                                   )
            data_feedback.save()
            return redirect('main:confirm')